package com.niit.example.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.niit.example.Calculator;

public class TestCalculator {

	private Calculator calculator;
	
	@Before
	public void setup() {
		calculator=new Calculator();
	}
	
	@After
	public void teardown() {
		calculator=null;
	}
	
	@Test
	public void testAdd() {
		assertEquals(10, calculator.add(10, 0));
	}
	
	@Test
	public void testAddNegative() {
		assertEquals(2, calculator.add(5, -3));
	}
	
	@Test
	public void testSubtract() {
		assertEquals(10, calculator.subtract(10, 0));
	}
	
	@Test
	public void testSubtractNegative() {
		assertEquals(8, calculator.subtract(5, -3));
	}
	
	@Test
	public void testMultiply() {
		assertEquals(0, calculator.multiply(10, 0));
	}
	
	@Test
	public void testMultiplyNegative() {
		assertEquals(-15, calculator.multiply(5, -3));
	}
	
	@Test
	public void testDivide() {
		assertEquals(5, calculator.divide(10, 2));
	}
	
	@Test(expected=ArithmeticException.class)
	public void testDivideByZero() {
		assertEquals(0, calculator.divide(10, 0));
	}
	
	@Test
	public void testDivideNegative() {
		assertEquals(-2, calculator.divide(6, -3));
	}

}
